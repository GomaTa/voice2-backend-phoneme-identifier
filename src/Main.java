import speakerIdentifier.ann.speakernet.mfccSupplier.ISupplier;
import speakerIdentifier.ann.speakernet.mfccSupplier.MfccSupplier;
import speakerIdentifier.ann.speakernet.SpeakerNet;
import speakerIdentifier.database.DB;
import speakerIdentifier.database.DBAudioFileHandler;
import speakerIdentifier.logger.HtmlFormatter;
import speakerIdentifier.serversockets.SpeakerIdentifierServer;
import speakerIdentifier.detection.Vector13D;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

/**
 * Created by GomaTa on 25.04.2016.
 */
public class Main {

    private static boolean exit = false;
    private static DB db;
    private static SpeakerNet speakerNet;
    private static String speakerNetData;
    private static SpeakerIdentifierServer pcs;
    private static ISupplier mfccSupplier;
    private static Logger logger;

    public static void main(String[] args) {
        initLogger();
        initDatabaseConnection();
        initTrainingsdata();
        initSpeakerNet();
        initPhonemeClassifierServer();
    }

    private static void initTrainingsdata() {
        try {
            DBAudioFileHandler dbAfh = new DBAudioFileHandler(db.getCon());
            File f = new File(Main.class.getClassLoader().getResource("trainingsdata.txt").toURI());
            String path = f.getParentFile().toString();
            System.out.println("[DB] Starting trainingsdata import");
            logger.info("[DB] Starting trainingsdata import");
            dbAfh.sqlInsertAudioFiles(path);
            System.out.println("[DB] Trainingsdata import done");
            logger.info("[DB] Trainingsdata import done");
        } catch (URISyntaxException e) {
            logger.warning(e.getMessage());
        }
    }

    private static void initPhonemeClassifierServer() {
        if(db.isConnected() && speakerNet != null) {
            pcs = new SpeakerIdentifierServer(db.getCon(), speakerNet, speakerNetData, mfccSupplier);
            pcs.start();
        }
    }

    private static void initSpeakerNet() {
        if(db.isConnected()) {
            try {
                mfccSupplier = new MfccSupplier(26);
                ArrayList<Vector13D> mfccs = mfccSupplier.getVectorsFromDB(db.getCon());
                String pathString = System.getProperty("java.io.tmpdir") + "Voice2" + File.separator;
                Path path = Paths.get(pathString);
                Files.createDirectories(path);
                speakerNet = new SpeakerNet(mfccs);
                speakerNetData = pathString + "mfcc.csv";
                speakerNet.saveWeightsToFile(speakerNetData);
                System.out.println("[SPEAKER-NET] Init of speaker net done.");
                logger.info("[SPEAKER-NET] Init of speaker net done.");
            } catch (IOException e) {
                logger.warning(e.getMessage());
            }
        }
    }

    private static void initLogger() {
        try {
            logger = Logger.getLogger(Main.class.getName());
            logger.setUseParentHandlers(false);
            String pathString = System.getProperty("java.io.tmpdir") + "Voice2" + File.separator + "speakerIdentifier" + File.separator;
            Path path = Paths.get(pathString);
            Files.createDirectories(path);
            FileHandler fileTxt = new FileHandler(pathString + new SimpleDateFormat("yy-MM-dd_HH-mm").format(new Date(System.currentTimeMillis())) + ".html");
            Formatter html = new HtmlFormatter();
            fileTxt.setFormatter(html);
            logger.addHandler(fileTxt);
        } catch (IOException e) {
            System.out.println("Unable to init logger: " + e.getMessage());
        }
    }

    private static void initDatabaseConnection(){
        db = new DB(logger);
        if(db.connect()){
            System.out.println("[DB] Successfully connected");
            logger.info("[DB] Successfully connected");
        }
    }
}
