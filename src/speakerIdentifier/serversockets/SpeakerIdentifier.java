package speakerIdentifier.serversockets;

import speakerIdentifier.ann.speakernet.mfccSupplier.ISupplier;
import speakerIdentifier.ann.speakernet.SpeakerNet;
import speakerIdentifier.detection.Vector13D;

import java.io.*;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by GomaTa on 03.05.2016.
 */
public class SpeakerIdentifier implements Runnable {

    private final Socket socket;
    private final Connection con;
    private final SpeakerNet speakerNet;
    private final ISupplier vectorSupplier;

    public SpeakerIdentifier(Socket socket, Connection con, SpeakerNet speakerNet, ISupplier vectorSupplier) {
        this.socket = socket;
        this.con = con;
        this.speakerNet = speakerNet;
        this.vectorSupplier = vectorSupplier;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String inputLine, requestId;
            if((inputLine = in.readLine()) != null) {
                requestId = inputLine;
                //check if id exists, etc.
                if(Integer.parseInt(requestId) > 0) {
                    File audiofile = getAudioFileFromDB(requestId);
                    if(audiofile != null) {
                        System.out.println("[PHONEM-CLASSIFIER] new request (id=" + requestId + ")");
                        Vector13D v = vectorSupplier.computeVectorOfFile(audiofile);
                        if(v != null) {
                            String speakerId = speakerNet.identify(v.getVector());
                            if(Integer.parseInt(speakerId) > 0){
                                System.out.println(speakerId);
                                insertName(requestId, speakerId);
                            }else{
                                System.out.println("speaker id not valid");
                            }
                        }else{
                            System.out.println("Vector null");
                        }
                    }else{
                        System.out.println("Audiofile null");
                    }
                }else{
                    System.out.println("[PHONEM-CLASSIFIER] Request id not valid.");
                }
            }
            in.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void insertName(String requestID, String speakerID) {
        try {
            con.setAutoCommit(false);
            String sql = "INSERT INTO detected_speaker (speaker_id, request_id) VALUES(?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, speakerID);
            stmt.setString(2, requestID);
            stmt.execute();
            con.commit();
            con.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private File getAudioFileFromDB(String hash) {
        ResultSet resultSet = null;
        File audiofile = null;
        try {
            con.setAutoCommit(false);
            //String sql = "SELECT request_id, audiofile FROM cutted_phoneme WHERE hash = ?;";
            String sql = "SELECT id, audiofile FROM request WHERE id = ?;";
            PreparedStatement getAudiofile = con.prepareStatement(sql);
            getAudiofile.setString(1, hash);
            resultSet = getAudiofile.executeQuery();
            con.commit();
            con.setAutoCommit(true);
            while (resultSet.next()) {
                String tempDir = System.getProperty("java.io.tmpdir");
                audiofile = new File(tempDir + "\\tmp_" + hash + ".wav");
                FileOutputStream fos = new FileOutputStream(audiofile);
                byte[] buffer = new byte[1];
                InputStream is = resultSet.getBinaryStream(2);
                while (is.read(buffer) > 0) {
                    fos.write(buffer);
                }
                fos.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return audiofile;
    }
}
