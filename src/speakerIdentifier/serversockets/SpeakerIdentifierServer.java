package speakerIdentifier.serversockets;

import speakerIdentifier.ann.speakernet.mfccSupplier.ISupplier;
import speakerIdentifier.ann.speakernet.SpeakerNet;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by GomaTa on 25.04.2016.
 */
public class SpeakerIdentifierServer extends Thread {

    private final Connection con;
    private final SpeakerNet speakerNet;
    private final String speakerNetData;
    private volatile boolean stop = false;
    private final int port = 16005;
    private final int numProc = 2;
    private final int maxRequests = 100;
    private int countRequests = 0;
    private ISupplier mfccSupplier;

    public SpeakerIdentifierServer(Connection con, SpeakerNet speakerNet, String speakerNetData, ISupplier mfccSupplier) {
        this.con = con;
        this.speakerNet = speakerNet;
        this.speakerNetData = speakerNetData;
        this.mfccSupplier =  mfccSupplier;
    }

    @Override
    public void run() {
        ServerSocket serverSocket = null;
        ExecutorService pool = Executors.newFixedThreadPool(numProc);
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("[PHONEM-CLASSIFIER-SERVER] Server is running...");
        } catch (IOException e) {
            System.err.println("[PHONEM-CLASSIFIER-SERVER] Could not listen on port: " + port + ".");
            System.exit(1);
        }
        Socket socket = null;
        while (!stop){
            try {
                socket = serverSocket.accept();;
                pool.submit(new SpeakerIdentifier(socket, con, speakerNet, mfccSupplier));
                countRequests++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        pool.shutdown();
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void terminate() {
        stop = true;
    }
}
