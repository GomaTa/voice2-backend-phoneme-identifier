package speakerIdentifier.mfcc.windowing;

/**
 * Created by GomaTa on 04.06.2016.
 */
public interface IWindowFunction {
    public double[] generate(final int windowSize);
}
