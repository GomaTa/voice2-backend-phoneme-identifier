package speakerIdentifier.mfcc.windowing;

import org.apache.commons.math3.util.FastMath;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 04.06.2016.
 */
public class HammingWindow implements IWindowFunction {

    @Override
    public double[] generate(final int windowSize) {
        return IntStream.range(0, windowSize).parallel().mapToDouble(
                i -> 0.54 - 0.46 * FastMath.cos((2 * FastMath.PI * i) / (windowSize - 1))
        ).toArray();
    }
}
