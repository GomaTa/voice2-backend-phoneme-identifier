package speakerIdentifier.mfcc;

import speakerIdentifier.audio.AudioStreamReader;
import speakerIdentifier.audio.framing.Framer;
import speakerIdentifier.audio.framing.IFramer;
import speakerIdentifier.audio.preemphasis.PreEmphasis;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by GomaTa on 13.06.2016.
 */
public class Mfcc implements IMfcc {
    private static final double DEFAULT_WINDOW_LENGTH = 0.050;
    private static final double DEFAULT_WINDOW_STEP = 0.025;
    private static final int DEFAULT_DFT_SIZE = 512;
    private static final double DEFAULT_PREEMPHASIS = 0.95;
    private static final double DEFAULT_LIFTERING = 22.0;

    @Override
    public double[] computeMfcc(File audioFile, double windowLength, double windowStep, int dftLength, double preEmphasisStrength, int numMfcc, double liferingAmount) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile);
            AudioStreamReader reader = new AudioStreamReader(audioInputStream);
            int sampleRate = (int) reader.getSampleRate();
            double[] samples = reader.readSamples();
            //Pre-emphasis
            PreEmphasis preEmphasis = new PreEmphasis();
            samples = preEmphasis.compute(samples, preEmphasisStrength);

            //Framing
            IFramer framer = new Framer();
            double[][] frames = framer.framing(samples, sampleRate, windowLength, windowStep);
            int numFrames = frames.length;
            //numFrames = 1;
            int numProcs = Runtime.getRuntime().availableProcessors();
            ExecutorService pool = Executors.newFixedThreadPool(numProcs);
            ArrayList<Future<MfccCoefficients>> future = new ArrayList<>();
            for (int i = 0; i < numFrames; i++) {
                Future<MfccCoefficients> f = pool.submit(new MfccCalculator(frames[i], sampleRate, dftLength, numMfcc, liferingAmount));
                future.add(f);
            }
            MfccCoefficients[] result = new MfccCoefficients[numFrames];
            for (int j = 0; j < numFrames; j++) {
                try {
                    Future<MfccCoefficients> f = future.get(j);
                    result[j] = f.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            pool.shutdown();
            return averageFrames(result);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        return null;
    }

    private double[] averageFrames(MfccCoefficients[] mfccFrames) {
        double[] avgMfcc = new double[mfccFrames[0].size()];
        int cnt = 0;
        for (int j = 0; j < mfccFrames.length; j++) {
            MfccCoefficients coefficients = mfccFrames[j];
            for (int i = 0; i < coefficients.size(); i++) {
                avgMfcc[i] += coefficients.get(i);
            }
            cnt++;
        }
        for (int i = 0; i < avgMfcc.length; i++) {
            avgMfcc[i] = avgMfcc[i] / cnt;
        }
        return avgMfcc;
    }

    @Override
    public double[] computeMFCC(final File audioFile, final int numMfcc) {
        return computeMfcc(audioFile, DEFAULT_WINDOW_LENGTH, DEFAULT_WINDOW_STEP, DEFAULT_DFT_SIZE, DEFAULT_PREEMPHASIS, numMfcc, DEFAULT_LIFTERING);
    }
}
