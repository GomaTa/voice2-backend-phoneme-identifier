package speakerIdentifier.mfcc.energyCalculation;

import org.apache.commons.math3.util.FastMath;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 18.05.2016.
 */
public class EnergyCalculator implements IEnergyCalculator {

    @Override
    public double[] calculateEnergies(double[] spectrum, double[][] filterbanks) {
        final double[] energies = new double[filterbanks.length];
        IntStream.range(0, energies.length).parallel().forEach(m -> { // for each filterbank
            double energy = 0;
            for (int k = 0; k < filterbanks[m].length; k++) {
                energy += spectrum[k] * filterbanks[m][k];
            }
            energies[m] = FastMath.log(energy);
        });
        return energies;
    }
}
