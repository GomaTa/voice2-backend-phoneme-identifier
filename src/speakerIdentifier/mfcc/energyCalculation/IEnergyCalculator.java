package speakerIdentifier.mfcc.energyCalculation;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface IEnergyCalculator {
    public double[] calculateEnergies(double[] spectrum, double[][] filterbanks);
}
