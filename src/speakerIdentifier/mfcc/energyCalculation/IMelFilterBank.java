package speakerIdentifier.mfcc.energyCalculation;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface IMelFilterBank {

    public double[][] generateMelFilterbank(final int amount, final double lowerFrequency, final double upperFrequency, final int sampleRate, final int filterLength);
}
