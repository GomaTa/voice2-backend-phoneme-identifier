package speakerIdentifier.mfcc.energyCalculation;

import org.apache.commons.math3.util.FastMath;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 17.05.2016.
 */
public class MelFilterBank implements IMelFilterBank {

    /**
     * Generates a certain amount of filter banks for a given frequency range.
     * @param amount The total number of filter banks to create.
     * @param lowerFrequency The lowest frequency the filter banks have to cover.
     * @param upperFrequency The highest frequency the filter banks have to cover.
     * @param sampleRate Sample rate for the audio samples.
     * @param filterLength The number of discrete points you want to calculate for each filter bank.
     * @return 2-Dimensional array. Each element represents a filter bank.
     */
    @Override
    public double[][] generateMelFilterbank(final int amount, final double lowerFrequency, final double upperFrequency, final int sampleRate, final int filterLength) {
        final double lowerMel = fromHerzToMel(lowerFrequency);
        final double upperMel = fromHerzToMel(upperFrequency);

        final double gap = (upperMel - lowerMel) / (amount + 1);
        final double[] points = new double[amount + 2];

        for (int i = 0; i < points.length; i++) {
            points[i] = FastMath.floor((filterLength + 1) * fromMelToHerz(lowerMel + i * gap) / sampleRate);
        }
        final double[][] filterbanks = new double[amount][filterLength];
        // m = filterbank index
        // k = point of a filterbank
        IntStream.range(0, filterbanks.length).parallel().forEach(m -> {
            for (int k = 0; k < filterbanks[m].length; k++) {
                if (k < points[m + 1 - 1]) {
                    filterbanks[m][k] = 0;
                } else if (k <= points[m + 1]) {
                    filterbanks[m][k] = (k - points[m + 1 - 1]) / (points[m + 1] - points[m + 1 - 1]);
                } else if (k <= points[m + 1 + 1]) {
                    filterbanks[m][k] = (points[m + 1 + 1] - k) / (points[m + 1 + 1] - points[m + 1]);
                } else {
                    filterbanks[m][k] = 0;
                }
            }
        });
        return filterbanks;
    }

    /**
     * Converts a frequency from Hertz to Mel
     *
     * @param hertz The frequency in Hertz
     */
    private double fromHerzToMel(final double hertz) {
        return 1125 * FastMath.log(1 + hertz / 700);
    }

    /**
     * Converts a frequency from Mel to Hertz
     *
     * @param mel The frequency in Mel
     */
    private double fromMelToHerz(final double mel) {
        return 700 * (FastMath.exp(mel / 1125) - 1);
    }


}
