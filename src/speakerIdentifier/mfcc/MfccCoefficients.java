package speakerIdentifier.mfcc;

import java.util.ArrayList;

/**
 * Created by GomaTa on 17.05.2016.
 */
public class MfccCoefficients {
    ArrayList<Double> coefficients;

    public MfccCoefficients(){
        coefficients = new ArrayList<>();
    }

    public ArrayList<Double> getCoefficients() {
        return coefficients;
    }

    public void add(double value){
        coefficients.add(value);
    }

    public double get(int index){
        return coefficients.get(index);
    }

    public int size(){
        return coefficients.size();
    }


}
