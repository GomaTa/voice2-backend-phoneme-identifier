package speakerIdentifier.mfcc.liftering;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface ILiftering {
    public double[] lift(double[] dctSpectrum, final double amount);
}
