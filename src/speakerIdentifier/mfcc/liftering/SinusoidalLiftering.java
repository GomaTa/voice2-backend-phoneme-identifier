package speakerIdentifier.mfcc.liftering;

import org.apache.commons.math3.util.FastMath;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 13.06.2016.
 */
public class SinusoidalLiftering implements ILiftering {


    @Override
    public double[] lift(double[] dctSpectrum, double amount) {
        double[] mfcc = new double[dctSpectrum.length];
        IntStream.range(0, dctSpectrum.length).parallel().forEach(i -> {
            final double lift = 1 + (amount / 2) * FastMath.sin(FastMath.PI * i / amount);
            mfcc[i] = lift * dctSpectrum[i];

        });
        return mfcc;
    }
}
