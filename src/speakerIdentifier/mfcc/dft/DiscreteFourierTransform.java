package speakerIdentifier.mfcc.dft;

import org.apache.commons.math3.util.FastMath;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 04.05.2016.
 */
public class DiscreteFourierTransform implements IDiscreteFourierTransform {
    /**
     * Computes the discrete fourier transformation
     *
     * @param samples Represents a window containing the particular audio samples
     * @param dftLength Size of the discrete fourier transformation
     * @return The computed spectrum of a window
     */
    @Override
    public double[] compute(final double[] samples, final int sampleRate, final int dftLength) {
        double[] spectrum = new double[dftLength];
        final int numWindowSamples = samples.length;
        IntStream.range(0, dftLength).forEach(k -> {
            double real = 0.0;
            double imaginary = 0.0;
            double freq = (k + 1) * sampleRate * 0.5 / Double.valueOf(dftLength);
            for (int n = 0; n < numWindowSamples; n++) {
                //k = index des spektrums
                //n = index der Sample
                // value = delta

                double value = 2 * FastMath.PI * freq * (n + 1) / Double.valueOf(numWindowSamples);
                real += samples[n] * FastMath.cos(value);
                imaginary += samples[n] * FastMath.sin(value);
            }
            spectrum[k] = (1.0 / numWindowSamples) * FastMath.sqrt((real * real) + (imaginary * imaginary));
            System.out.printf(Locale.GERMAN,"[k=%.4f]real:%.4f imag:%.4f ampl:%.4f\n",freq,real,imaginary,spectrum[k]);
        });
        return spectrum;
    }

    /**
     * Saves windows and their samples to a file. Each line contains the samples of the particular window.
     * Values are separated by the delimiter symbol ";"
     * @param filepath Path to file
     * @param samples  Audio samples
     */
    public static void saveWindows(String filepath, double[][] samples) {
        try {
            List<String> lines = new LinkedList<>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < samples.length; i++) {
                for (int j = 0; j < samples[i].length; j++) {
                    sb.append(String.format("%.2f;", samples[i][j]));
                }
                sb.append("\n");
            }
            lines.add(sb.toString());
            Files.write(Paths.get(filepath), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves audio samples to a file. Values are separated by the delimiter symbol ";"
     * @param filepath   Path to file
     * @param sampleRate Sample rate for the audio samples
     * @param dftLength  Size of the discrete fourier transformation
     * @param samples    Audio samples
     */
    public static void saveSamplesToFile(String filepath, double sampleRate, double dftLength, double[] samples) {
        try {
            List<String> lines = new LinkedList<>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < samples.length; i++) {
                double freq = ((i + 1) / dftLength) * (sampleRate * 0.5);
                sb.append(String.format("%.2f;%f;\n", freq, samples[i]));
            }
            lines.add(sb.toString());
            Files.write(Paths.get(filepath), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
