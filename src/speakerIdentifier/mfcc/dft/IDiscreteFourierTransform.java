package speakerIdentifier.mfcc.dft;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface IDiscreteFourierTransform {
    public double[] compute(final double[] samples, int sampleRate, final int dftLength);
}
