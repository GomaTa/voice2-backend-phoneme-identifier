package speakerIdentifier.mfcc.dct;

import org.apache.commons.math3.util.FastMath;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 18.05.2016.
 */
public class DiscreteCosineTranform implements IDiscreteCosineTransform {

    /**
     * Calculates the discrete cosine transform for given energy values.
     * @param energies Energies calculated by the mel filter banks.
     * @param dctLength Length of the discrete cosine transform.
     * @return Values of the discrete cosine transform.
     */
    @Override
    public double[] compute(final double[] energies, final int dctLength) {
        final double[] dct = new double[dctLength];
        final int N = energies.length;
        IntStream.range(0, dct.length).parallel().forEach(k -> {
            double sum = 0;
            for (int n = 0; n < N; n++) {
                sum += energies[n] * FastMath.cos((FastMath.PI / N) * (n + 0.5) * k);
            }
            dct[k] = sum;
        });
        return dct;
    }
}
