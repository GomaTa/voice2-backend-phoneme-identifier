package speakerIdentifier.mfcc.dct;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface IDiscreteCosineTransform {
    public double[] compute(final double[] energies, final int dctLength);
}
