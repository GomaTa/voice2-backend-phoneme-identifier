package speakerIdentifier.mfcc;

import speakerIdentifier.mfcc.windowing.HammingWindow;
import speakerIdentifier.mfcc.windowing.IWindowFunction;
import speakerIdentifier.mfcc.dct.DiscreteCosineTranform;
import speakerIdentifier.mfcc.dct.IDiscreteCosineTransform;
import speakerIdentifier.mfcc.dft.DiscreteFourierTransform;
import speakerIdentifier.mfcc.dft.IDiscreteFourierTransform;
import speakerIdentifier.mfcc.energyCalculation.EnergyCalculator;
import speakerIdentifier.mfcc.energyCalculation.IEnergyCalculator;
import speakerIdentifier.mfcc.energyCalculation.IMelFilterBank;
import speakerIdentifier.mfcc.energyCalculation.MelFilterBank;
import speakerIdentifier.mfcc.liftering.ILiftering;
import speakerIdentifier.mfcc.liftering.SinusoidalLiftering;

import java.util.concurrent.Callable;

/**
 * Created by GomaTa on 17.05.2016.
 */
public class MfccCalculator implements Callable<MfccCoefficients> {

    private final double[] samples;
    private final int numMfccCoefficients;
    private final int dftLength;
    private final int sampleRate;
    private final double liftAmount;

    public MfccCalculator(final double[] samples, final int sampleRate, final int dftLength, final int numMfccCoefficients, final double liftAmount) {
        this.samples = samples;
        this.sampleRate = sampleRate;
        this.dftLength = dftLength;
        this.numMfccCoefficients = numMfccCoefficients;
        this.liftAmount = liftAmount;
    }

    @Override
    public MfccCoefficients call() throws Exception {
        try{
            //Apply HammingWindow
            IWindowFunction windowFunction = new HammingWindow();
            double[] hammingWindow = windowFunction.generate(samples.length);
            for(int i = 0; i < hammingWindow.length; i++){
                samples[i] = hammingWindow[i] * samples[i];
            }
            // Apply the discrete fourier transform to get a spectrum
            IDiscreteFourierTransform dft = new DiscreteFourierTransform();
            double[] dftSpectrum = dft.compute(samples, sampleRate, dftLength);
            // Apply a mel filterbank on the spectrum and calculate the amount of energy in each filter
            IMelFilterBank melFilterBank = new MelFilterBank();
            double[][] melFilterBanks = melFilterBank.generateMelFilterbank(numMfccCoefficients, 300, 8000, sampleRate, dftLength);
            IEnergyCalculator energyCalculator = new EnergyCalculator();
            double[] energies = energyCalculator.calculateEnergies(dftSpectrum, melFilterBanks);
            // Apply a discrete cosine transform to get the mel frequency cepstral coefficients
            IDiscreteCosineTransform dct = new DiscreteCosineTranform();
            double[] dctSpectrum = dct.compute(energies, numMfccCoefficients);
            // Apply cepstral liftering on the result
            ILiftering liftering = new SinusoidalLiftering();
            double[] mfcc = liftering.lift(dctSpectrum, liftAmount);
            MfccCoefficients coefficients = new MfccCoefficients();
            for(int i = 0; i < mfcc.length; i++){
                coefficients.add(mfcc[i]);
            }
            return coefficients;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new MfccCoefficients();
    }
}
