package speakerIdentifier.mfcc;

import java.io.File;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface IMfcc {

    public double[] computeMfcc(final File audioFile, final double windowLength, final double windowStep, final int dftLength, final double preEmphasis, final int numMfcc, final double liferingAmount);

    public double[] computeMFCC(final File audioFile, final int numMfcc);
}
