package speakerIdentifier.visualization;

/**
 * Created by GomaTa on 09.06.2016.
 */
public enum AudioChartUnit {
    NUMBER(""),
    HERTZ("Hz"),
    DECIBEL("db"),
    SAMPLES("samples"),
    MS("ms");

    private final String name;

    AudioChartUnit(String name) {
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
