package speakerIdentifier.visualization;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by GomaTa on 09.06.2016.
 */
abstract class AudioChart {
    protected double minX;
    protected double maxX;
    protected double minY;
    protected double maxY;
    protected double stepX;
    protected double stepY;
    protected AudioChartUnit unitX;
    protected AudioChartUnit unitY;
    protected BufferedImage image;
    protected Graphics2D g;
    protected Color backgroundColor = Color.WHITE;
    protected int imageWidth;
    protected int imageHeight;
    protected int graphLineThickness = 5;
    protected int axisLineThickness = 5;
    protected Color graphLineColor = Color.BLUE;
    protected Color axisLineColor = Color.BLACK;
    protected double pixelPerPointY;
    protected double pixelPerPointX;
    protected Font font;
    protected int margin = 50;

    public abstract void draw(String filepath, double[] data, double minX, double maxX, double minY, double maxY, double stepX, double stepY, AudioChartUnit unitX, AudioChartUnit unitY);

}
