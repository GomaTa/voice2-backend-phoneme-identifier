package speakerIdentifier.visualization;

import org.apache.commons.math3.util.FastMath;
import speakerIdentifier.audio.AudioStreamReader;
import speakerIdentifier.audio.framing.Framer;
import speakerIdentifier.audio.preemphasis.IPreEmphasis;
import speakerIdentifier.audio.preemphasis.PreEmphasis;
import speakerIdentifier.mfcc.dft.DiscreteFourierTransform;
import speakerIdentifier.mfcc.windowing.HammingWindow;
import speakerIdentifier.mfcc.windowing.IWindowFunction;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by GomaTa on 10.06.2016.
 */
public class SpectrogramChart extends AudioChart {
    private double dataOffset;

    public void draw(String filepath, double[] data, double dataOffset, double minX, double maxX, double minY, double maxY, double stepX, double stepY, AudioChartUnit unitX, AudioChartUnit unitY) {
        this.dataOffset = dataOffset;
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.stepX = stepX;
        this.stepY = stepY;
        this.unitX = unitX;
        this.unitY = unitY;
        try {
            margin = 150;
            imageWidth = 5000;
            imageHeight = (int) (imageWidth / 16.0 * 9.0);
            pixelPerPointY = (imageHeight - 2 * margin) / FastMath.abs(maxY - minY);
            pixelPerPointX = (imageWidth - 2 * margin) / maxX;
            font = new Font("Arial", Font.PLAIN, (int) (imageHeight * 0.01));
            axisLineThickness = 5;
            graphLineThickness = 3;
            image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            g = image.createGraphics();
            g.setColor(backgroundColor);
            g.fillRect(0, 0, imageWidth, imageHeight);
            g.setStroke(new BasicStroke(graphLineThickness));
            g.setFont(font);
            drawXAxis();
            drawYAxis();
            drawData(data);
            ImageIO.write(image, "PNG", new File(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void drawData(double[] data) {
        g.setStroke(new BasicStroke(graphLineThickness));
        g.setColor(graphLineColor);
        for (int i = 0; i < data.length; i++) {
            int x = (int) (margin + (i + 1) * dataOffset * pixelPerPointX);
            int y1 = imageHeight - margin;
            int y2;
            if (data[i] <= minY) {
                y2 = y1;
            } else {
                y2 = (int) (imageHeight - margin - (data[i] - minY) * pixelPerPointY);
            }
            g.drawLine(x, y1, x, y2);
        }

    }

    private void drawXAxis() {
        g.setStroke(new BasicStroke(axisLineThickness));
        g.setColor(axisLineColor);
        g.drawLine(margin, imageHeight - margin, imageWidth - margin, imageHeight - margin);
        g.drawLine((int) (imageWidth - margin - 60 * pixelPerPointX), (int) (imageHeight - margin - 30 * pixelPerPointX), imageWidth - margin, imageHeight - margin);
        g.drawLine((int) (imageWidth - margin - 60 * pixelPerPointX), (int) (imageHeight - margin + 30 * pixelPerPointX), imageWidth - margin, imageHeight - margin);

        for (int i = 0; i < maxX; i += stepX) {
            int x = (int) (pixelPerPointX * i + margin);
            int y1 = (int) (imageHeight - margin - 10 * pixelPerPointX);
            int y2 = (int) (imageHeight - margin + 10 * pixelPerPointX);
            g.drawLine(x, y1, x, y2);
            g.drawString(String.valueOf(i), (int) (x - margin / 5.0), (int) (y2 + imageHeight * 0.015));
        }

        g.drawString(unitX.toString(), imageWidth - margin / 2, imageHeight - margin / 2);

    }

    private void drawYAxis() {
        g.setStroke(new BasicStroke(axisLineThickness));
        g.setColor(axisLineColor);
        g.drawLine(margin, margin, margin, imageHeight - margin);
        g.drawLine((int) (margin - 30 * pixelPerPointX), (int) (margin + 60 * pixelPerPointX), margin, margin);
        g.drawLine((int) (margin + 30 * pixelPerPointX), (int) (margin + 60 * pixelPerPointX), margin, margin);
        for (double i = minY; i < maxY; i += stepY) {
            int x1 = (int) (margin - 10 * pixelPerPointX);
            int x2 = (int) (margin + 10 * pixelPerPointX);
            int y = (int) (imageHeight - margin - pixelPerPointY * (i - minY));
            g.drawLine(x1, y, x2, y);
            g.drawString(String.format(Locale.US, "%.0f", i), (int) (margin / 2.0), y);
        }
        g.drawString(unitY.toString(), margin / 2, margin / 2);
    }

    public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
        SpectrogramChart sm = new SpectrogramChart();


        /*AudioInputStream ais = AudioSystem.getAudioInputStream(new File("D:\\freq20.wav"));
        AudioStreamReader asr = new AudioStreamReader(ais);
        double[] samples = asr.readSamples();
        IPreEmphasis preEmphasis = new PreEmphasis();
        //samples = preEmphasis.compute(samples, 0.95);

        Framer framer = new Framer();
        double[][] framedSignal = framer.framing(samples, 2205, 1102);
        double[][] framedSpectrum = new double[framedSignal.length][];

        IWindowFunction windowFunction = new HammingWindow();
        double[] hammingWindow = windowFunction.generate(2205);
        for(int i = 0; i <framedSignal.length; i++){
            for(int j = 0; j <framedSignal[i].length; j++){
                framedSignal[i][j] = framedSignal[i][j] * hammingWindow[j];
            }
        }
        int dftLenght = 512;
        int sampleRate = (int) asr.getSampleRate();
        DiscreteFourierTransform dft = new DiscreteFourierTransform();
        for(int i = 0; i < framedSignal.length; i++){
            framedSpectrum[i] = dft.compute(framedSignal[i], sampleRate, dftLenght);
        }
        double[] spectrum = new double[framedSpectrum[0].length];
        for(int i = 0; i < framedSpectrum[0].length; i++){
            double value = 0.0D;
            for(int j = 0; j < framedSpectrum.length; j++){
                value += framedSpectrum[j][i];
            }
            spectrum[i] = value;
        }

        DecibelConverter dbConverter = new DecibelConverter();
        double[] dbSpectrum = dbConverter.convert(spectrum);
        double freqGap = sampleRate * 0.5 / dftLenght;
        sm.draw("D:\\freq20SpectrogramHwOn.png", dbSpectrum, freqGap, 0, 24000+500, -80, 0, 1000, 10, AudioChartUnit.HERTZ, AudioChartUnit.DECIBEL);
        */

        //Windowing
        /*
        double[] frame1Samples = framer.framing(samples, 2205, 1102)[5];
        double[] frame1Spectrum = dft.compute(frame1Samples, sampleRate, dftLenght);
        double[] frame1DecibelSpectrum = dbConverter.convert(frame1Spectrum);
        sm.draw("D:\\freqSpectrogramWindowingHammingWindowOff.png", frame1DecibelSpectrum, freqGap, 0, 24000+500, -120, 0, 1000, 10, AudioChartUnit.HERTZ, AudioChartUnit.DECIBEL);

        for(int i = 0; i < frame1Samples.length; i++){
            frame1Samples[i] = frame1Samples[i] * hammingWindow[i];
        }
        double[] frame1SpectrumHwOn = dft.compute(frame1Samples, sampleRate, dftLenght);
        double[] frame1DecibelSpectrumHwOn = dbConverter.convert(frame1SpectrumHwOn);
        sm.draw("D:\\freqSpectrogramWindowingHammingWindowOn.png", frame1DecibelSpectrumHwOn, freqGap, 0, 24000+500, -120, 0, 1000, 10, AudioChartUnit.HERTZ, AudioChartUnit.DECIBEL);
        */
    }

    @Override
    public void draw(String filepath, double[] data, double minX, double maxX, double minY, double maxY, double stepX, double stepY, AudioChartUnit unitX, AudioChartUnit unitY) {

    }
}
