package speakerIdentifier.visualization;

import org.apache.commons.math3.util.FastMath;

/**
 * Created by GomaTa on 15.06.2016.
 */
public class DecibelConverter {

    public double[] convert(double[] spectrum){
        double[] dbSpectrum = new double[spectrum.length];
        for(int i = 0; i < dbSpectrum.length; i++){
            dbSpectrum[i] = 20 * FastMath.log10(spectrum[i] / 1.0);
        }
        return dbSpectrum;
    }
}
