package speakerIdentifier.visualization;

import speakerIdentifier.mfcc.energyCalculation.MelFilterBank;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by GomaTa on 09.06.2016.
 */
public class MelFilterBankChart extends AudioChart {

    @Override
    public void draw(String filepath, double[] data, double minX, double maxX, double minY, double maxY, double stepX, double stepY, AudioChartUnit unitX, AudioChartUnit unitY) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.stepX = stepX;
        this.stepY = stepY;
        this.unitX = unitX;
        this.unitY = unitY;
        try {
            margin = (int) (maxX * 0.02);
            margin = 100;
            //imageWidth = (int) (maxX + 2 * margin);
            imageWidth = 5000;
            imageHeight = (int) (imageWidth / 16.0 * 9.0);
            pixelPerPointY = (imageHeight - 2 * margin) / maxY;
            pixelPerPointX = (imageWidth - 2 * margin) / maxX;
            font = new Font("Arial", Font.PLAIN, (int) (75 * pixelPerPointX));
            image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            axisLineThickness = 5;
            graphLineThickness = 3;
            g = image.createGraphics();
            g.setColor(backgroundColor);
            g.fillRect(0, 0, imageWidth, imageHeight);
            g.setStroke(new BasicStroke(graphLineThickness));
            g.setFont(font);
            drawXAxis();
            drawYAxis();
            drawData(data);
            ImageIO.write(image, "PNG", new File(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void drawData(double[] data) {
        g.setStroke(new BasicStroke(graphLineThickness));
        g.setColor(graphLineColor);
        for (int i = 0; i < data.length - 2; i++) {
            int startPointX = (int) (margin + data[i] * pixelPerPointX);
            int startPointY = imageHeight - margin;
            int endPointX = (int) (margin + data[i + 2] * pixelPerPointX);
            int endPointY = imageHeight - margin;
            int middlePointX = (int) (startPointX + (data[i + 2] - data[i]) / 2 * pixelPerPointX);
            int middlePointY = (int) (imageHeight - pixelPerPointY * 1);
            g.drawLine(startPointX, startPointY, middlePointX, middlePointY);
            g.drawLine(middlePointX, middlePointY, endPointX, endPointY);
        }

    }

    private void drawXAxis() {
        g.setStroke(new BasicStroke(axisLineThickness));
        g.setColor(axisLineColor);
        g.drawLine(margin, imageHeight - margin, imageWidth - margin, imageHeight - margin);
        g.drawLine((int) (imageWidth - margin - 60 * pixelPerPointX), (int) (imageHeight - margin - 30 * pixelPerPointX), imageWidth - margin, imageHeight - margin);
        g.drawLine((int) (imageWidth - margin - 60 * pixelPerPointX), (int) (imageHeight - margin + 30 * pixelPerPointX), imageWidth - margin, imageHeight - margin);

        for (int i = 0; i < maxX; i += stepX) {
            int x = (int) (pixelPerPointX * i + margin);
            int y1 = (int) (imageHeight - margin - 10 * pixelPerPointX);
            int y2 = (int) (imageHeight - margin + 10 * pixelPerPointX);
            g.drawLine(x, y1, x, y2);
            g.drawString(String.valueOf(i), (int) (x - margin / 2.0), (int) (y2 + margin / 2.0));
        }

        g.drawString(unitX.toString(), imageWidth - margin / 2, imageHeight - margin / 2);

    }

    private void drawYAxis() {
        g.setStroke(new BasicStroke(axisLineThickness));
        g.setColor(axisLineColor);
        g.drawLine(margin, margin, margin, imageHeight - margin);
        g.drawLine((int) (margin - 30 * pixelPerPointX), (int) (margin + 60 * pixelPerPointX), margin, margin);
        g.drawLine((int) (margin + 30 * pixelPerPointX), (int) (margin + 60 * pixelPerPointX), margin, margin);
        for (double i = stepY; i < maxY; i += stepY) {
            int x1 = (int) (margin - 10 * pixelPerPointX);
            int x2 = (int) (margin + 10 * pixelPerPointX);
            int y = (int) (imageHeight - pixelPerPointY * i);
            g.drawLine(x1, y, x2, y);
            g.drawString(String.format(Locale.US, "%.1f", i), (int) (margin / 5.0), (int) (y + margin / 5.0));
        }
        g.drawString(unitY.toString(), margin / 2, margin / 2);
    }

    public static void main(String[] args) {
        MelFilterBankChart mfb = new MelFilterBankChart();
        mfb.draw("D:\\melfilterbank.png", new double[]{300, 517.33, 781.90, 1103.97, 1496.04, 1973.32, 2554.33, 3261.62, 4122.63, 5170.76, 6446.70, 8000}, 0, 8500, 0, 1.1, 500, 0.5, AudioChartUnit.HERTZ, AudioChartUnit.NUMBER);
    }

}
