package speakerIdentifier.visualization;

import org.apache.commons.math3.util.FastMath;
import speakerIdentifier.audio.AudioStreamReader;
import speakerIdentifier.mfcc.dft.DiscreteFourierTransform;
import speakerIdentifier.mfcc.windowing.HammingWindow;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by GomaTa on 10.06.2016.
 */
public class AudiosignalChart extends AudioChart {
    private double drawPixelPerPoint = 1.0;

    @Override
    public void draw(String filepath, double[] data, double minX, double maxX, double minY, double maxY, double stepX, double stepY, AudioChartUnit unitX, AudioChartUnit unitY) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.stepX = stepX;
        this.stepY = stepY;
        this.unitX = unitX;
        this.unitY = unitY;
        try {
            margin = 400;
            imageWidth = 8000;
            imageHeight = (int) (imageWidth / 16.0 * 4.0);
            pixelPerPointY = (imageHeight - 2 * margin) / FastMath.abs(maxY - minY);
            pixelPerPointX = (imageWidth - 2 * margin) / maxX;
            drawPixelPerPoint = imageWidth * 0.001;
            font = new Font("Arial", Font.PLAIN, (int) (6 * drawPixelPerPoint));
            axisLineThickness = 5;
            graphLineThickness = 3;
            image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            g = image.createGraphics();
            g.setColor(backgroundColor);
            g.fillRect(0, 0, imageWidth, imageHeight);
            g.setFont(font);
            drawXAxis();
            drawYAxis();
            drawData(data);
            ImageIO.write(image, "PNG", new File(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void draw(String filepath, double[][] data, double minX, double maxX, double minY, double maxY, double stepX, double stepY, AudioChartUnit unitX, AudioChartUnit unitY) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.stepX = stepX;
        this.stepY = stepY;
        this.unitX = unitX;
        this.unitY = unitY;
        try {
            margin = 400;
            imageWidth = 8000;
            imageHeight = (int) (imageWidth / 16.0 * 4.0);
            pixelPerPointY = (imageHeight - 2 * margin) / FastMath.abs(maxY - minY);
            pixelPerPointX = (imageWidth - 2 * margin) / maxX;
            drawPixelPerPoint = imageWidth * 0.001;
            font = new Font("Arial", Font.PLAIN, (int) (6 * drawPixelPerPoint));
            axisLineThickness = 5;
            graphLineThickness = 3;
            image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            g = image.createGraphics();
            g.setColor(backgroundColor);
            g.fillRect(0, 0, imageWidth, imageHeight);
            g.setFont(font);
            drawXAxis();
            drawYAxis();
            drawData(data);
            ImageIO.write(image, "PNG", new File(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void drawData(double[][] data) {
        Color red = new Color(193, 0, 0);
        Color[] c = new Color[]{red, red, graphLineColor};
        g.setStroke(new BasicStroke(graphLineThickness));
        g.setColor(graphLineColor);
        int baseY = imageHeight / 2;
        for (int i = 0; i < data.length; i++) {
            g.setColor(c[i]);
            int prevX = margin;
            int prevY = (int) (baseY - data[i][0] * pixelPerPointY);
            for (int j = 0; j < data[i].length; j++) {
                int x = (int) (margin + (j + 1) * pixelPerPointX);
                int y1 = baseY;
                int y2 = (int) (baseY - data[i][j] * pixelPerPointY);
                //g.drawLine(x, y1, x, y2);
                g.drawLine(prevX, prevY, x, y2);
                prevX = x;
                prevY = y2;
            }
        }

    }

    private void drawData(double[] data) {
        g.setStroke(new BasicStroke(graphLineThickness));
        g.setColor(graphLineColor);
        int baseY = imageHeight / 2;
        int prevX = margin;
        int prevY = (int) (baseY - data[0] * pixelPerPointY);
        for (int i = 0; i < data.length; i++) {
            int x = (int) (margin + (i + 1) * pixelPerPointX);
            int y1 = baseY;
            int y2 = (int) (baseY - data[i] * pixelPerPointY);
            //g.drawLine(x, y1, x, y2);
            g.drawLine(prevX, prevY, x, y2);
            prevX = x;
            prevY = y2;
        }

    }

    private void drawXAxis() {
        g.setStroke(new BasicStroke(axisLineThickness));
        g.setColor(axisLineColor);
        int baseY = imageHeight / 2;
        g.drawLine(margin, baseY, imageWidth - margin, baseY);
        g.drawLine((int) (imageWidth - margin - 3 * drawPixelPerPoint), (int) (baseY - 2 * drawPixelPerPoint), imageWidth - margin, baseY);
        g.drawLine((int) (imageWidth - margin - 3 * drawPixelPerPoint), (int) (baseY + 2 * drawPixelPerPoint), imageWidth - margin, baseY);

        g.drawString("0", (int) (margin - drawPixelPerPoint * 5), (int) (baseY + drawPixelPerPoint * 8));
        for (int i = (int) stepX; i < maxX; i += stepX) {
            int x = (int) (pixelPerPointX * i + margin);
            int y1 = (int) (baseY - 1 * drawPixelPerPoint);
            int y2 = (int) (baseY + graphLineThickness + 1 * drawPixelPerPoint);
            g.drawLine(x, y1, x, y2);
            String str = String.valueOf(i);
            g.drawString(str, (int) (x - drawPixelPerPoint * (1.5 + 0.5 * str.length())), (int) (baseY + drawPixelPerPoint * 8));
        }
        g.drawString(unitX.toString(), (int) (imageWidth - margin / 1.25), baseY);
    }


    private void drawYAxis() {
        g.setStroke(new BasicStroke(axisLineThickness));
        g.setColor(axisLineColor);
        g.drawLine(margin, margin, margin, imageHeight - margin);
        //g.drawLine((int) (margin - 2 * drawPixelPerPoint), (int) (margin + 3 * drawPixelPerPoint), margin, margin);
        //g.drawLine((int) (margin + 2 * drawPixelPerPoint), (int) (margin + 3 * drawPixelPerPoint), margin, margin);
        for (double i = minY; i <= maxY; i += stepY) {
            int x1 = (int) (margin - 1 * drawPixelPerPoint);
            int x2 = (int) (margin + 1 * drawPixelPerPoint);
            int y = (int) (imageHeight - margin - pixelPerPointY * (i - minY));
            g.drawLine(x1, y, x2, y);
            if (i != 0.0) {
                g.drawString(String.format(Locale.US, "%.1f", i), (int) (margin / 2.0), y);
            }
        }
        g.drawString(unitY.toString(), margin / 2, margin / 2);
    }

    public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
        AudiosignalChart asc = new AudiosignalChart();

        //Frequency cos
        AudioInputStream ais = AudioSystem.getAudioInputStream(new File("D:\\cos10.wav"));
        AudioStreamReader asr = new AudioStreamReader(ais);
        double[] samples = asr.readSamples();
        asc.draw("D:\\cos10.png", samples, 0, samples.length + 1, -1, 1, 1000, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        //Frequency sin
        for(int i = 1; i <= 8; i++){
            AudioInputStream aisSin = AudioSystem.getAudioInputStream(new File("D:\\sin"+i+".wav"));
            AudioStreamReader asrSin = new AudioStreamReader(aisSin);
            double[] samplesSin = asrSin.readSamples();
            asc.draw("D:\\sin"+i+".png", samplesSin, 0, samplesSin.length + 1, -1, 1, 1000, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);
        }

        AudioInputStream aisSin = AudioSystem.getAudioInputStream(new File("D:\\sinGesamt.wav"));
        AudioStreamReader asrSin = new AudioStreamReader(aisSin);
        double[] samplesSin = asrSin.readSamples();
        asc.draw("D:\\sinsinGesamt.png", samplesSin, 0, samplesSin.length + 1, -1, 1, 1000, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        //System.exit(1);
        //Frequency 2,3

        /*
        //Framing

        AudioInputStream aisFraming = AudioSystem.getAudioInputStream(new File("D:\\A.wav"));
        AudioStreamReader asrFraming = new AudioStreamReader(aisFraming);
        double[] samplesFraming = asrFraming.readSamples();
        asc.draw("D:\\framing.png", samplesFraming, 0, samplesFraming.length + 1, -1, 1, 1000, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);


        //Frame #5
        double[] samplesFrame5 = new double[2400];
        int startFrame = 4 * 1200;
        for (int i = 0; i < samplesFrame5.length; i++) {
            samplesFrame5[i] = samplesFraming[startFrame + i];
        }
        asc.draw("D:\\frame5.png", samplesFrame5, 0, samplesFrame5.length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        //Hamming Window

        HammingWindow hw = new HammingWindow();
        double[] hwSamples = hw.generate(samplesFrame5.length);
        //asc.draw("D:\\hammingWindow.png", hwSamples, 0, hwSamples[0].length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        //Windowing
        double[][] windowing = new double[3][];
        windowing[0] = hwSamples;
        //windowing[1] = hwSamples[1];
        windowing[2] = samplesFrame5;
        //asc.draw("D:\\windowingOff.png", windowing, 0, windowing[0].length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);
        windowing[2] = new double[samplesFrame5.length];
        for (int i = 0; i < samplesFrame5.length; i++) {
            windowing[2][i] = samplesFrame5[i] * hwSamples[i];

        }
        System.out.printf(Locale.GERMAN,"%f\n",windowing[2][0]);
        System.out.printf(Locale.GERMAN,"%f\n",windowing[2][1]);
        System.out.printf(Locale.GERMAN,"%f\n",windowing[2][2]);
        System.out.println();
        System.out.printf(Locale.GERMAN,"%f\n",windowing[2][2397]);
        System.out.printf(Locale.GERMAN,"%f\n",windowing[2][2398]);
        System.out.printf(Locale.GERMAN,"%f\n",windowing[2][2399]);

        //asc.draw("D:\\windowingOn.png", windowing, 0, windowing[0].length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        DiscreteFourierTransform dft = new DiscreteFourierTransform();
        dft.compute(windowing[2],48000,1920);
        System.exit(1);

        //Sinus 25Hz
        double[] samplesSinus = new double[samplesFrame5.length];
        double freq = 25;
        double samplerate = samplesFrame5.length;
        for (int i = 0; i < samplesSinus.length; i++) {
            samplesSinus[i] = FastMath.sin((i+1) * (2 * Math.PI * freq / samplerate));
        }
        asc.draw("D:\\sinus25Hertz.png", samplesSinus, 0, samplesSinus.length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        //Cosinus 25Hz
        double[] samplesCosine = new double[samplesFrame5.length];
        for (int i = 0; i < samplesSinus.length; i++) {
            samplesCosine[i] = FastMath.cos((i+1) * (2 * Math.PI * freq / samplerate));
        }
        asc.draw("D:\\cosine25Hertz.png", samplesCosine, 0, samplesCosine.length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);

        //Multply frame with sinus and cosine 25Hz
        double[] samplesFrame5Sinus = new double[samplesFrame5.length];
        double[] samplesFrame5Cosine = new double[samplesFrame5.length];
        for (int i = 0; i < samplesSinus.length; i++) {
            samplesFrame5Sinus[i] = samplesSinus[i] * samplesFrame5[i];
            samplesFrame5Cosine[i] = samplesCosine[i] * samplesFrame5[i];
        }
        asc.draw("D:\\frame5sinus.png", samplesFrame5Sinus, 0, samplesFrame5Sinus.length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);
        asc.draw("D:\\frame5cosine.png", samplesFrame5Cosine, 0, samplesFrame5Cosine.length + 1, -1, 1, 100, 0.5, AudioChartUnit.SAMPLES, AudioChartUnit.NUMBER);
        */
    }
}
