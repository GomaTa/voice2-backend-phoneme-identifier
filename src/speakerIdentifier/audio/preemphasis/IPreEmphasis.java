package speakerIdentifier.audio.preemphasis;

/**
 * Created by GomaTa on 14.06.2016.
 */
public interface IPreEmphasis {

    public double[] compute(final double[] samples, final double preemphasisStrength);
}
