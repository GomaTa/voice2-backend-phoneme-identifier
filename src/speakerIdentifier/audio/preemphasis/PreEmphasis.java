package speakerIdentifier.audio.preemphasis;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 18.05.2016.
 */
public class PreEmphasis implements IPreEmphasis {

    /**
     * Applies a preempasis filter to given {@code samples}.
     *
     * @param samples             Audio samples.
     * @param preemphasisStrength Srength of the pre emphasis filter.
     * @return Samples filtered by the pre emphasis filter.
     */
    @Override
    public double[] compute(final double[] samples, final double preemphasisStrength) {
        final double[] newSignal = new double[samples.length];
        newSignal[0] = samples[0];
        IntStream.range(1, newSignal.length).parallel().forEach(i -> {
            newSignal[i] = samples[i] - (preemphasisStrength * samples[i - 1]);
        });
        return newSignal;
    }
}
