package speakerIdentifier.audio.framing;

/**
 * Created by GomaTa on 13.06.2016.
 */
public interface IFramer {
    public double[][] framing(final double[] samples, final int sampleRate, final double frameLength, final double frameStep);

    public double[][] framing(final double[] samples, final int frameLength, final int frameStep);
}
