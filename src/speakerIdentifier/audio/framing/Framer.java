package speakerIdentifier.audio.framing;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 04.06.2016.
 */
public class Framer implements IFramer{
    /**
     * Generates windows (short blocks) using audio samples.
     * @param samples Audio samples
     * @param sampleRate Sample rate for the audio samples
     * @param frameLength Window length in seconds
     * @param frameStep Offset between two windows in seconds
     * @return 2-Dimensional array. Each element represents a generated window containing the particular audio samples.
     */
    public double[][] framing(final double[] samples, final int sampleRate, final double frameLength, final double frameStep){
        final int frameSampleCount = (int) (frameLength * sampleRate);
        final int offsetSampleCount = (int) (frameStep * sampleRate);
        final int framesCount = getFrameCount(samples.length, frameSampleCount, offsetSampleCount);

        final double[][] framedSignal = new double[framesCount][frameSampleCount];
        IntStream.range(0, framedSignal.length).parallel().forEach(i -> {
            final int startOffset = i * offsetSampleCount;
            IntStream.range(0, framedSignal[i].length).parallel().forEach(j -> {
                if (startOffset + j < samples.length)
                    framedSignal[i][j] = samples[startOffset + j];
            });
        });
        return framedSignal;
    }

    /**
     * Generates windows (short blocks) using audio samples.
     * @param samples Audio samples
     * @param frameLength Window length in seconds
     * @param frameStep Offset between two windows in seconds
     * @return 2-Dimensional array. Each element represents a generated window containing the particular audio samples.
     */
    @Override
    public double[][] framing(final double[] samples, final int frameLength, final int frameStep){
        final int framesCount = getFrameCount(samples.length, frameLength, frameStep);

        final double[][] framedSignal = new double[framesCount][frameLength];
        IntStream.range(0, framedSignal.length).parallel().forEach(i -> {
            final int startOffset = i * frameStep;
            IntStream.range(0, framedSignal[i].length).parallel().forEach(j -> {
                if (startOffset + j < samples.length)
                    framedSignal[i][j] = samples[startOffset + j];
            });
        });
        return framedSignal;
    }

    /**
     * Caluclates the amount of windows for giving audio samples.
     * @param sampleCount Amount of audio samples
     * @param windowLength Window length in samples
     * @param windowStep Offset between two windows in samples
     * @return
     */
    private int getFrameCount(final int sampleCount, final int windowLength, final int windowStep) {
        int framesCount = 1;
        for (int i = sampleCount - windowLength; i > 0; i-= windowStep) {
            framesCount++;
        }
        return framesCount;
    }
}
