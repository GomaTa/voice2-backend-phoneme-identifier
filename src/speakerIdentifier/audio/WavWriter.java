package speakerIdentifier.audio;

import org.apache.commons.math3.util.FastMath;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 09.05.2016.
 */
public class WavWriter {

    private final int sampleRate;
    private final int bitSize;
    private final int numChannels;
    private final double[] samples;
    private final int headerLength = 44;
    private int fileSize = 0;

    public WavWriter(int sampleRate, int bitSize, int numChannels, double[] samples) {
        this.sampleRate = sampleRate;
        this.bitSize = bitSize;
        this.numChannels = numChannels;
        this.samples = samples;
        System.out.printf("bitSize: %d\n", bitSize);
        System.out.printf("sampleRate: %d\n", sampleRate);
        System.out.printf("numSample: %d\n", samples.length);
        System.out.printf("numChannels: %d\n", numChannels);

    }

    private byte[] toByteArray(int value, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = (byte) (i >> ((len - i - 1) * 8));
        }
        return tmp;
    }

    private byte[] toByteArray(String str, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            if (i < str.length()) {
                tmp[i] = (byte) str.charAt(i);
            } else {
                tmp[i] = 0x00;
            }

        }
        return tmp;
    }

    private byte[] StringToByteArray(String str) {
        byte[] tmp = new byte[str.length()];
        for (int i = 0; i < str.length(); i++) {
            tmp[i] = (byte) str.charAt(i);
        }
        return tmp;
    }

    private byte[] IntToByteArray(int value, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = (byte) (i >> ((len - i - 1) * 8));
        }
        return tmp;
    }

    private byte[] IntTo4ByteArray(int value) {
        byte[] tmp = new byte[4];
        tmp[0] = (byte) (value);
        tmp[1] = (byte) (value >> 8);
        tmp[2] = (byte) (value >> 16);
        tmp[3] = (byte) (value >> 24);
        return tmp;
    }

    private byte[] IntTo2ByteArray(int value) {
        byte[] tmp = new byte[2];
        tmp[0] = (byte) (value);
        tmp[1] = (byte) (value >> 8);
        return tmp;
    }

    private byte[] doubleTo16BitArray(double value) {
        // saturation
        value = Math.min(1.0, Math.max(-1.0, value));
        // scaling and conversion to integer
        int nSample = (int) Math.round(value * 32767.0);
        byte high = (byte) ((byte) (nSample >> 8) & 0xFF);
        byte low = (byte) (nSample & 0xFF);
        return new byte[]{low, high};
    }


    private void copyDataToByteArray(byte[] bFile, byte[] bData, int startIndex) {
        if (bFile.length < (startIndex + bData.length)) {
            throw new IndexOutOfBoundsException();
        }
        for (int i = 0; i < bData.length; i++) {
            bFile[startIndex + i] = bData[i];
        }
    }

    public void writeFile(String filepath) {
        FileOutputStream fileOuputStream = null;
        try {
            fileSize = headerLength + (samples.length * (bitSize / 8));
            System.out.printf("FileSize: %d\n", fileSize);
            byte[] bFile = new byte[fileSize];
            //RIFF marker
            copyDataToByteArray(bFile, StringToByteArray("RIFF"), 0);
            //file-size (equals file-size - 8)
            copyDataToByteArray(bFile, IntTo4ByteArray(fileSize - 8), 4);
            //WAVE marker
            copyDataToByteArray(bFile, StringToByteArray("WAVE"), 8);
            //Format marker
            copyDataToByteArray(bFile, StringToByteArray("fmt "), 12);
            //Length of format data (always 16)
            copyDataToByteArray(bFile, IntTo4ByteArray(16), 16);
            //Wave type PCM
            copyDataToByteArray(bFile, IntTo2ByteArray(1), 20);
            //Number channels
            copyDataToByteArray(bFile, IntTo2ByteArray(numChannels), 22);
            //Sample Rate
            copyDataToByteArray(bFile, IntTo4ByteArray(sampleRate), 24);
            //(Sample Rate * Bit Size * Channels) / 8
            copyDataToByteArray(bFile, IntTo4ByteArray((sampleRate * bitSize * numChannels) / 8), 28);
            //(Bit Size * Channels) / 8
            copyDataToByteArray(bFile, IntTo2ByteArray((bitSize * numChannels) / 8), 32);
            //Bits per sample
            copyDataToByteArray(bFile, IntTo2ByteArray(bitSize), 34);
            //data marker
            copyDataToByteArray(bFile, StringToByteArray("data"), 36);
            //data-size (equals file-size - 44)
            copyDataToByteArray(bFile, IntTo4ByteArray(fileSize - headerLength), 40);
            for (int i = 0; i < samples.length; i++) {
                byte[] sample = doubleTo16BitArray(samples[i]);
                copyDataToByteArray(bFile, sample, headerLength + (bitSize / 8) * i);
            }
            fileOuputStream = new FileOutputStream(filepath);
            fileOuputStream.write(bFile);
            fileOuputStream.close();
            System.out.println("Done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static double[] preEmphasisFilter(final double[] samples, double preemphasis) {
        final double[] newSignal = new double[samples.length];
        newSignal[0] = samples[0];
        IntStream.range(1, newSignal.length).parallel().forEach(i -> {
            newSignal[i] = samples[i] - (preemphasis * samples[i - 1]);
        });
        return newSignal;
    }

    public static void main(String[] args) {
        File audioFile = new File("C:\\Users\\GomaTa\\Documents\\VOICE_Prototyp\\resources\\A_Eric_Zenker_02.wav");
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(audioFile);
            AudioStreamReader reader = new AudioStreamReader(audioInputStream);
            //double[] samples = reader.readSamples();
            //pre emphasis filter
            //samples = preEmphasisFilter(samples, 0.95);
            double[] samples = new double[48000];
            double sampleRate = 48000.0;
            int k = 8;
            for (int i = 0; i < samples.length; i++) {
                samples[i] = FastMath.sin(i * (2 * Math.PI * 1 / sampleRate))
                        + 0.5 * FastMath.sin(i * (2 * Math.PI * 2 / sampleRate))
                        + 0.3 * FastMath.sin(i * (2 * Math.PI * 3 / sampleRate))
                        + 0.75 * FastMath.sin(i * (2 * Math.PI * 4 / sampleRate))
                        + FastMath.sin(i * (2 * Math.PI * 5 / sampleRate))
                        + 0.5 * FastMath.sin(i * (2 * Math.PI * 6 / sampleRate))
                        + FastMath.sin(i * (2 * Math.PI * 7 / sampleRate))
                        + FastMath.sin(i * (2 * Math.PI * 8 / sampleRate));
                samples[i] = 1.0 / 8 * samples[i];
                //samples[i] = FastMath.cos((Math.PI / sampleRate * (i+0.5) * k));
            }
            //samples = preEmphasisFilter(samples, 0.95);
            WavWriter ww = new WavWriter((int) sampleRate, 16, 1, samples);
            ww.writeFile("D:\\sinGesamt.wav");
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

