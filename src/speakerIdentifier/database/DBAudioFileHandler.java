package speakerIdentifier.database;

import speakerIdentifier.ann.speakernet.mfccSupplier.ISupplier;
import speakerIdentifier.ann.speakernet.mfccSupplier.MfccSupplier;
import speakerIdentifier.detection.Vector13D;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by GomaTa on 25.04.2016.
 */
public class DBAudioFileHandler {
    private Connection con;

    public DBAudioFileHandler(Connection con) {
        this.con = con;
    }

    public String getFileChecksum(MessageDigest digest, File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }
        fis.close();
        byte[] bytes = digest.digest();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }


    public String calculateMD5(File file) {
        try {
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");
            return getFileChecksum(md5Digest, file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sqlInsertAudiofile(String md5Hash, String firstname, String lastname, String phoneme, String mfcc, File file) {
        String speakerID = createNewSpeaker(firstname, lastname);
        insertTrainingsSpeakerData(md5Hash, speakerID, phoneme, mfcc, file);
    }

    private String createNewSpeaker(final String firstname, final String lastname) {
        try {
            con.setAutoCommit(false);
            String speakerID;
            PreparedStatement stmt = con.prepareStatement("SELECT id FROM speaker WHERE firstname = ? AND lastname = ?");
            stmt.setString(1, firstname);
            stmt.setString(2, lastname);
            ResultSet tmpSpeakerID = stmt.executeQuery();
            if (tmpSpeakerID.next()) {
                speakerID = tmpSpeakerID.getString(1);
            } else {
                String sql = "INSERT INTO speaker (firstname, lastname) VALUES (?, ?)";
                PreparedStatement stmt1 = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                stmt1.setString(1, firstname);
                stmt1.setString(2, lastname);
                stmt1.executeUpdate();
                con.commit();
                ResultSet rs = stmt1.getGeneratedKeys();
                rs.next();
                speakerID = rs.getString(1);
                rs.close();
            }
            con.setAutoCommit(true);
            return speakerID;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void insertTrainingsSpeakerData(final String md5Hash, final String speakerID, final String phoneme, final String mfcc, final File file) {
        try {
            con.setAutoCommit(false);
            String sqlInsert = "INSERT INTO trainings_speaker_data (hash, speaker_id, phoneme, mfcc, audiofile) VALUES (?, ?, ? , ? ,?)";
            PreparedStatement stmtInsert = con.prepareStatement(sqlInsert);
            stmtInsert.setString(1, md5Hash);
            stmtInsert.setString(2, speakerID);
            stmtInsert.setString(3, phoneme);
            stmtInsert.setString(4, mfcc);
            FileInputStream fis = new FileInputStream(file);
            stmtInsert.setBinaryStream(5, fis, (int) file.length());
            stmtInsert.execute();
            con.commit();
            con.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void sqlInsertAudioFiles(String path) {
        try {
            File dir = new File(path);
            Pattern p = Pattern.compile("(A|E|I|O|U)_(.+)_(.+)_.*wav");
            String md5hash, firstname, lastname, phoneme, mfcc;
            fileLoop:
            for (File f : dir.listFiles()) {
                if (!f.isDirectory() && f.getName().matches("(A|E|I|O|U)_.*_.*_.*wav")) {
                    Matcher m = p.matcher(f.getName());
                    if (m.matches()) {
                        md5hash = calculateMD5(f);
                        if (!trainingsFileExists(md5hash)) {
                            phoneme = m.group(1);
                            firstname = m.group(2);
                            lastname = m.group(3);
                            mfcc = calculateMfcc(f);
                            sqlInsertAudiofile(md5hash, firstname, lastname, phoneme, mfcc, f);
                        }
                    }
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private boolean trainingsFileExists(String md5Hash) {
        try {
            String sql = "SELECT hash FROM trainings_speaker_data WHERE hash = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, md5Hash);
            ResultSet tmpSpeakerID = stmt.executeQuery();
            if (tmpSpeakerID.next()) {
                if (md5Hash.equals(tmpSpeakerID.getString(1))) {
                    return true;
                }
            }
            //con.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String calculateMfcc(File f) {
        ISupplier mfccSupplier = new MfccSupplier(26);
        Vector13D vector = mfccSupplier.computeVectorOfFile(f);
        if(vector == null){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (double value : vector.getVector()) {
            sb.append(String.format(Locale.US, "%f;", value));
        }
        return sb.toString();
    }

    public File getAudioFileFromDB() {
        ResultSet resultSet = null;
        File audiofile = null;
        try {
            String sql = "SELECT hash, audiofile FROM test_data LIMIT 1";
            PreparedStatement stmt = con.prepareStatement(sql);
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                String hash = resultSet.getString(1);
                String tempDir = System.getProperty("java.io.tmpdir");
                audiofile = new File(tempDir + "\\" + hash + ".wav");
                FileOutputStream fos = new FileOutputStream(audiofile);
                byte[] buffer = new byte[1];
                InputStream is = resultSet.getBinaryStream(2);
                while (is.read(buffer) > 0) {
                    fos.write(buffer);
                }
                fos.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return audiofile;
    }
}
