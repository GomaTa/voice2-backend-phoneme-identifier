package speakerIdentifier.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by GomaTa on 23.04.2016.
 */
public class DB {
    private Connection con;
    private boolean status = false;
    private String dbname;
    private String username;
    private String password;
    private Logger logger;

    public DB(Logger logger) {
        this.logger = logger;
        try {
            Properties p = new Properties();
            String propFilename = "config/config.properties";
            InputStream inputStream = DB.class.getClassLoader().getResourceAsStream(propFilename);
            p.load(inputStream);
            this.dbname = p.getProperty("dbname");
            this.username = p.getProperty("dbusername");
            this.password = p.getProperty("dbpassword");
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }

    public boolean connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost/voice2";
            con = DriverManager.getConnection(url, "voice2", "dhbwmosbach");
            status = true;
            return true;
        } catch (InstantiationException e) {
            logger.warning(e.getMessage());
        } catch (IllegalAccessException e) {
            logger.warning(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.warning(e.getMessage());
        } catch (SQLException e) {
            logger.warning(e.getMessage());
        }
        return false;
    }

    public Connection getCon() {
        return con;
    }

    public boolean isConnected() {
        return status;
    }
}
