package speakerIdentifier.ann.activationFunctions;

import org.apache.commons.math3.util.FastMath;

public class TanH implements IActivationFunction {

    @Override
    public double[] function(double[] z) {
        double[] f = new double[z.length];
        for(int i = 0; i < f.length; i++){
            f[i] = function(z[i]);
        }
        return f;
    }

    @Override
    public double[] derivation(double[] z){
        double[] d = new double[z.length];
        for(int i = 0; i < d.length; i++){
            d[i] = derivation(z[i]);
        }
        return d;
    }

    @Override
    public double derivation(double z) {
        return 1.0D - function(z) * function(z);
    }

    @Override
    public double function(double z){
        return FastMath.tanh(z);
    }
}
