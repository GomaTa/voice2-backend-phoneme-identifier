package speakerIdentifier.ann.activationFunctions;

public interface IActivationFunction {
    double[] function(double[] z);

    double function(double z);

    double[] derivation(double[] z);

    double derivation(double z);
}
