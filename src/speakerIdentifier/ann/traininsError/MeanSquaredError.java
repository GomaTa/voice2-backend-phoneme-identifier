package speakerIdentifier.ann.traininsError;

/**
 * Created by GomaTa on 26.03.2016.
 */
public class MeanSquaredError implements ITrainingsError{

    @Override
    public double calculateError(double[] idealOutput, double[] actualOutput) {
        double tmpError = 0.0;
        for(int i = 0; i < idealOutput.length; i++){
            tmpError += Math.pow(idealOutput[i]-actualOutput[i],2);
        }
        return 0.5 * tmpError;
    }
}
