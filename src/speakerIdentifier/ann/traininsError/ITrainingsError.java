package speakerIdentifier.ann.traininsError;

/**
 * Created by GomaTa on 15.06.2016.
 */
public interface ITrainingsError {
    public double calculateError(double[] idealOutput, double[] actualOutput);
}
