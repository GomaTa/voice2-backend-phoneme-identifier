package speakerIdentifier.ann;

import speakerIdentifier.ann.autoencoder.AudioAutoencoder;
import speakerIdentifier.ann.autoencoderTrainer.AudioAutoencoderTrainer;
import speakerIdentifier.ann.autoencoderTrainer.FileSupplier;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Created by GomaTa on 15.06.2016.
 */
public class AutoencoderPrototype {
    public static void main(String[] args) {
        try {
            AudioAutoencoder aae = new AudioAutoencoder();
            AudioAutoencoderTrainer aaet = new AudioAutoencoderTrainer(aae, 0.02, 2400);
            FileSupplier fs = new FileSupplier();
            File[] trainingsData = fs.getFiles(new File(AutoencoderPrototype.class.getClassLoader().getResource("trainingsdata.txt").toURI()).getParentFile().toString());
            aaet.train(trainingsData, 200, 1);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
