package speakerIdentifier.ann.autoencoderTrainer;


import speakerIdentifier.ann.autoencoder.AudioAutoencoder;
import speakerIdentifier.ann.autoencoder.AutoencoderDataHandler;
import speakerIdentifier.ann.autoencoder.components.*;
import speakerIdentifier.ann.traininsError.ITrainingsError;
import speakerIdentifier.ann.traininsError.MeanSquaredError;
import speakerIdentifier.audio.AudioStreamReader;
import speakerIdentifier.audio.framing.Framer;
import speakerIdentifier.audio.framing.IFramer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by GomaTa on 18.04.2016.
 */
public class AudioAutoencoderTrainer implements IAutoencoderTrainer {

    private final int countLayers;
    private final int frameSampleSize;
    private boolean DEBUG = true;

    private final AudioAutoencoder autoencoder;
    private final double learningRate;
    private ITrainingsError trainingsError;
    private AutoencoderDataHandler dataHandler = new AutoencoderDataHandler();
    private boolean showresult;

    public AudioAutoencoderTrainer(AudioAutoencoder autoencoder, double learningRate, int frameSampleSize) {
        this.autoencoder = autoencoder;
        this.learningRate = learningRate;
        this.frameSampleSize = frameSampleSize;
        this.trainingsError = new MeanSquaredError();
        this.countLayers = autoencoder.getCountLayers();
    }


    @Override
    public void train(File[] trainingsData, int epochCount, int miniBatchSize) {
        this.showresult = showresult;
        if (autoencoder.isBuild()) {
            double[][][] trainingsSampledData = new double[trainingsData.length][][];
            for (int i = 0; i < trainingsData.length; i++) {
                trainingsSampledData[i] = framing(trainingsData[i]);
            }
            if(miniBatchSize > 1){
                //Mini-Batch Gradient Descent
            }else {
                StochasticGradientDescent(trainingsSampledData, epochCount);
            }
        } else {
            //throw new AutoEncoderException("Autoencoder not built.");
        }
    }


    private void StochasticGradientDescent(double[][][] trainingsData, final int numEpoch) {
        int epoch = 1;
        Random r = new Random();
        do {
            int index1 = r.nextInt(trainingsData.length);
            int index2 = r.nextInt(trainingsData[index1].length);
            double[] subset = trainingsData[index1][index2];
            update(subset);
            if (DEBUG) {
                System.out.printf("[DEBUG] (%d/%d) - Mean Squared Error: %f\n", epoch, numEpoch, trainingsError.calculateError(subset, autoencoder.getOutputLayer().getActivations()));
            }
            epoch++;
        } while (epoch <= numEpoch);
    }

    private double[][] shuffleTrainingsData(double[][][] trainingsSampleData, int length) {
        double[][] subset = new double[length][];
        Random r = new Random();
        HashMap<String, Boolean> indexList = new HashMap<>();
        int index1, index2;
        for (int i = 0; i < length; i++) {
            index1 = r.nextInt(trainingsSampleData.length);
            index2 = r.nextInt(trainingsSampleData[index1].length);
            while (indexList.get(index1 + "_" + index2) != null) {
                index1 = r.nextInt(trainingsSampleData.length);
                index2 = r.nextInt(trainingsSampleData[index1].length);
            }
            indexList.put(index1 + "_" + index2, true);
            subset[i] = trainingsSampleData[index1][index2];
        }
        return subset;
    }

    private void update(double[] subset) {
        NablaBiases[] nablaB = new NablaBiases[countLayers - 1];
        NablaWeights[] nablaW = new NablaWeights[countLayers - 1];
        for (int i = 1; i < countLayers; i++) {
            Layer layer = autoencoder.getLayer(i);
            Layer prevLayer = autoencoder.getLayer(i - 1);
            nablaB[i - 1] = new NablaBiases(layer.getCountNodes());
            nablaW[i - 1] = new NablaWeights(layer.getCountNodes(), prevLayer.getCountNodes());
        }
        backpropagation(subset, nablaW, nablaB);
        updateBiases(nablaB);
        updateWeights(nablaW);

    }

    public void updateBiases(NablaBiases[] nablaB) {
        for (int l = 1; l < countLayers; l++) {
            Layer layer = autoencoder.getLayer(l);
            ArrayList<Node> nodes = layer.getNodes();
            NablaBiases nablaBLayer = nablaB[l - 1];
            for (int j = 0; j < nodes.size(); j++) {
                Node node = nodes.get(j);
                double oldValue = node.getBias();
                node.setBias(oldValue - (learningRate * nablaBLayer.getValue(j)));
            }
        }
    }

    public void updateWeights(NablaWeights[] nablaW) {
        for (int l = 1; l < countLayers; l++) {
            Layer layer = autoencoder.getLayer(l);
            ArrayList<Node> nodes = layer.getNodes();
            NablaWeights nablaWLayer = nablaW[l - 1];
            for (int j = 0; j < nodes.size(); j++) {
                Node node = nodes.get(j);
                ArrayList<Double> nablaWNode = nablaWLayer.getWeightsOfNode(j);
                double[] oldWeights = node.getWeights();
                for (int k = 0; k < oldWeights.length; k++) {
                    node.updateWeight(k, oldWeights[k] - (learningRate * nablaWNode.get(k)));
                }
            }
        }
    }

    public void backpropagation(double[] idealValues, NablaWeights[] nablaW, NablaBiases[] nablaB) {
        //feed forward
        autoencoder.feedForward(idealValues);
        if (showresult) {
            Layer outputLayer = autoencoder.getOutputLayer();
            for (int i = 0; i < outputLayer.getCountNodes(); i++) {
                System.out.printf("%f|%f\n", idealValues[i], outputLayer.getNodes().get(i).getActivationValue());
            }
        }
        Layer prevLayer, currentLayer, nextLayer;
        Delta deltaClass;

        currentLayer = autoencoder.getOutputLayer();
        prevLayer = autoencoder.getLayer(countLayers - 2);
        // calculate delta for output layer
        deltaClass = new Delta(currentLayer.getActivationFunction());
        double[] delta = deltaClass.calculate(idealValues, currentLayer.getActivations(), currentLayer.getZs());

        nablaW[countLayers - 2].setWeights(delta, prevLayer.getActivations());
        nablaB[countLayers - 2].setBiases(delta);

        //calculate nabla w and b for other layers
        for (int i = countLayers - 2; i > 0; i--) {
            currentLayer = autoencoder.getLayer(i);
            nextLayer = autoencoder.getLayer(i + 1);
            prevLayer = autoencoder.getLayer(i - 1);

            // calculate deltas
            deltaClass = new Delta(currentLayer.getActivationFunction());
            delta = deltaClass.calculate(nextLayer.getWeights(), delta, currentLayer.getZs());

            // calculate delta nabla weights and biases
            nablaW[i - 1].setWeights(delta, prevLayer.getActivations());
            nablaB[i - 1].setBiases(delta);
        }
    }

    private double[][] framing(File audioFile) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile);
            AudioStreamReader reader = new AudioStreamReader(audioInputStream);
            //return automatically normalised double values
            double[] samples = reader.readSamples();
            IFramer framer = new Framer();
            double[][] framedSamples = framer.framing(samples, frameSampleSize, frameSampleSize / 2);
            return framedSamples;
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int getWindowCount(final double[] samples, final int windowSampleCount, final int offsetSampleCount) {
        int windowCount = 1;
        for (int i = samples.length - windowSampleCount; i > 0; i -= offsetSampleCount) {
            windowCount++;
        }
        return windowCount;
    }
}
