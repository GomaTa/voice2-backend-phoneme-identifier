package speakerIdentifier.ann.autoencoderTrainer;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by GomaTa on 15.06.2016.
 */
public class FileSupplier {

    public File[] getFiles(String folderpath){
        File dir = new File(folderpath);
        ArrayList<File> files = new ArrayList<>();
        fileLoop:
        for (File f : dir.listFiles()) {
            if (!f.isDirectory() && f.getName().matches("(A|E|I|O|U)_.*_.*_.*wav") ) {
                files.add(f);
            }
        }
        File[] result = new File[files.size()];
        for(int i = 0; i < result.length; i++){
            result[i] = files.get(i);
        }
        return result;
    }

}
