package speakerIdentifier.ann.autoencoderTrainer;

import java.io.File;

/**
 * Created by GomaTa on 15.06.2016.
 */
public interface IAutoencoderTrainer {

    public void train(File[] trainingsData, int epochCount, int miniBatchSize);
}
