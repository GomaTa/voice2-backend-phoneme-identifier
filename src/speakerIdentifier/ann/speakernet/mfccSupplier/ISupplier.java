package speakerIdentifier.ann.speakernet.mfccSupplier;

import speakerIdentifier.detection.Vector13D;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by GomaTa on 14.06.2016.
 */
public interface ISupplier {

    public Vector13D computeVectorOfFile(String filepath);

    public Vector13D computeVectorOfFile(File audiofile);

    public ArrayList<Vector13D> computeVectorsOfFolder(String folderpath);

    public ArrayList<Vector13D> getVectorsFromDB(Connection con);

}
