package speakerIdentifier.ann.speakernet.mfccSupplier;

import speakerIdentifier.detection.Vector13D;
import speakerIdentifier.mfcc.Mfcc;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MfccSupplier implements ISupplier {
    private final int numMfcc;

    public MfccSupplier(final int numMfcc) {
        this.numMfcc = numMfcc;
    }

    /**
     * The function computeMFCOfFile computes mfcc data from a file.
     *
     * @param filepath Path of the file.
     */
    public Vector13D computeVectorOfFile(String filepath) {
        File audiofile = new File(filepath);
        return computeVectorOfFile(audiofile);
    }

    public Vector13D computeVectorOfFile(File audiofile) {
        Mfcc mfcc = new Mfcc();
        double[] mfccVector = mfcc.computeMFCC(audiofile, numMfcc);
        if(mfccVector != null) {
            String fileName = audiofile.getName(), vocal = "UNKNOWN", person = "UNKNOWN";
            Pattern p = Pattern.compile("(A|E|I|O|U)_(.+)_(.+)_.*wav");
            Matcher m = p.matcher(fileName);
            if (m.matches()) {
                vocal = m.group(1);
                person = m.group(2) + " " + m.group(3);
            }
            return new Vector13D(mfccVector, vocal, person);
        }else{
            return null;
        }
    }

    /**
     * The function computeMFCOfFolder computes mfcc data from a folder.
     *
     * @param folderpath Path of the folder.
     */
    @Override
    public ArrayList<Vector13D> computeVectorsOfFolder(String folderpath) {
        File dir = new File(folderpath);
        ArrayList<Vector13D> output = new ArrayList<>();
        fileLoop:
        for (File p : dir.listFiles()) {
            if (!p.isDirectory() && p.getName().matches("(A|E|I|O|U)_.*_.*_.*wav")) {
                Vector13D v = computeVectorOfFile(p.toString());
                output.add(v);
            }
        }
        return output;
    }

    @Override
    public ArrayList<Vector13D> getVectorsFromDB(Connection con) {
        ResultSet resultSet = null;
        ArrayList<Vector13D> vectors = new ArrayList<>();
        try {
            String sql = "SELECT speaker_id, phoneme, mfcc FROM trainings_speaker_data";
            PreparedStatement stmt = con.prepareStatement(sql);
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                String speakerID = resultSet.getString(1);
                String phonem = resultSet.getString(2);
                String mfccString = resultSet.getString(3);
                String[] mfccStrArray = mfccString.split(";");
                double[] mfcc = new double[mfccStrArray.length];
                for (int i = 0; i < mfccStrArray.length; i++) {
                    mfcc[i] = Double.valueOf(mfccStrArray[i].replace(",", "."));
                }
                vectors.add(new Vector13D(mfcc, phonem, speakerID));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vectors;
    }

}
