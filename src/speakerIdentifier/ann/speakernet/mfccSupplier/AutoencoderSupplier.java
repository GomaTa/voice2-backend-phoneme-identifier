package speakerIdentifier.ann.speakernet.mfccSupplier;


import speakerIdentifier.ann.autoencoder.AudioAutoencoder;
import speakerIdentifier.detection.Vector13D;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by GomaTa on 23.04.2016.
 */
public class AutoencoderSupplier implements ISupplier {

    private final AudioAutoencoder audioAutoencoder;

    public AutoencoderSupplier(AudioAutoencoder audioAutoencoder) {
        this.audioAutoencoder = audioAutoencoder;
    }

    @Override
    public Vector13D computeVectorOfFile(String filepath) {
        File audiofile = new File(filepath);
        return computeVectorOfFile(audiofile);
    }

    @Override
    public Vector13D computeVectorOfFile(File audiofile) {
        double[] encodeValues = audioAutoencoder.encode(audiofile);
        String fileName = audiofile.getName(), vocal = "UNKNOWN", person = "UNKNOWN";
        Pattern p = Pattern.compile("(A|E|I|O|U)_(.+)_(.+)_.*wav");
        Matcher m = p.matcher(fileName);
        if (m.matches()) {
            vocal = m.group(1);
            person = m.group(2) + " " + m.group(3);
        }
        return new Vector13D(encodeValues, vocal, person);
    }

    /**
     * The function computeMFCOfFolder computes mfcc data from a folder.
     *
     * @param path Path of the folder.
     */
    @Override
    public ArrayList<Vector13D> computeVectorsOfFolder(String path) {
        File dir = new File(path);
        ArrayList<Vector13D> output = new ArrayList<>();
        fileLoop:
        for (File p : dir.listFiles()) {
            if (!p.isDirectory() && p.getName().matches("(A|E|I|O|U)_.*_.*_.*wav")) {
                Vector13D v = computeVectorOfFile(p.toString());
                output.add(v);
            }
        }
        return output;
    }

    @Override
    public ArrayList<Vector13D> getVectorsFromDB(Connection con) {
        return null;
    }
}
