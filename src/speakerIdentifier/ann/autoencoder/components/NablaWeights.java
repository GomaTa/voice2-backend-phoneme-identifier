package speakerIdentifier.ann.autoencoder.components;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 19.04.2016.
 */
public class NablaWeights {
    private final int countWeights;
    private final int countNodes;
    private ArrayList<ArrayList<Double>> weights = new ArrayList<>();

    public NablaWeights(int countNodes, int countWeights){
        this.countNodes = countNodes;
        this.countWeights = countWeights;
        init();
    }

    private void init(){
        for(int i = 0; i < countNodes; i++){
            ArrayList<Double> tmpWeights = new ArrayList<>();
            for(int j = 0; j < countWeights; j++){
                tmpWeights.add(0.0);
            }
            weights.add(tmpWeights);
        }
    }

    public void setWeights(final double[] delta, final double[] prevActivationValues){
        //deltaNablaW[j][k] = delta nalba for node
        // a[k] = activation value of node of current layer
        // d[j] = delta of node of next layer
        IntStream.range(0,delta.length).parallel().forEach(j ->
            IntStream.range(0,prevActivationValues.length).parallel().forEach(k ->
                    weights.get(j).set(k, (prevActivationValues[k] * delta[j]))
            )
        );
    }

    public ArrayList<Double> getWeightsOfNode(int index){
        return weights.get(index);
    }
}
