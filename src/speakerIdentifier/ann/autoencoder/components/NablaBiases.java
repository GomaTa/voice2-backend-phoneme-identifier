package speakerIdentifier.ann.autoencoder.components;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 19.04.2016.
 */
public class NablaBiases {
    private ArrayList<Double> biases = new ArrayList<>();
    private int countBiases;

    public NablaBiases(int countBiases){
        this.countBiases = countBiases;
        init();
    }

    private void init(){
        for(int i = 0; i < countBiases; i++){
            biases.add(0.0);
        }
    }

    public void setBiases(final double[] values){
        IntStream.range(0,countBiases).parallel().forEach(j -> biases.set(j, values[j]));
    }

    public double getValue(int index){
        return biases.get(index);
    }
}
