package speakerIdentifier.ann.autoencoder.components;

import speakerIdentifier.ann.activationFunctions.IActivationFunction;

import java.util.stream.IntStream;

/**
 * Created by GomaTa on 19.04.2016.
 */
public class Delta {
    private final IActivationFunction activationFunction;
    private double[] values;

    public Delta(IActivationFunction activationFunction) {
        this.activationFunction = activationFunction;
    }

    public double[] calculate(double[] idealValues, double[] actualValues, double[] zValues) {
        return IntStream.range(0, idealValues.length).parallel().mapToDouble(
                j -> (actualValues[j] - idealValues[j]) * activationFunction.derivation(zValues[j])).toArray();
    }

    public double[] calculate(final double[][] nextWeights, final double[] nextDelta, final double[] zValues) {
        double[] delta = new double[zValues.length];
        IntStream.range(0, zValues.length).parallel().forEach(j -> {
            double sum = IntStream.range(0, nextDelta.length).parallel().mapToDouble(k -> nextWeights[k][j] * nextDelta[k]).sum();
            delta[j] = sum * activationFunction.derivation(zValues[j]);
        });
        return delta;
    }
}
