package speakerIdentifier.ann.autoencoder.components;

import speakerIdentifier.ann.activationFunctions.IActivationFunction;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 19.04.2016.
 */
public class Node {
    private final int id;
    private final IActivationFunction activationFunction;
    private double bias;
    private ArrayList<Double> weights = new ArrayList<>();
    private final int countWeights;
    private double activationValue;
    private double zValue;

    public Node(int id, int countWeights, IActivationFunction activationFunction) {
        this.id = id;
        this.countWeights = countWeights;
        this.activationFunction = activationFunction;
        init();
    }

    private void init() {
        bias = 0.0;
        for (int i = 0; i < countWeights; i++) {
            weights.add(0.0001 * Math.random());
            //weights.add(Math.random());
        }
    }

    public int getId() {
        return id;
    }

    public double getBias() {
        return bias;
    }

    public double[] getWeights() {
        double[] weights = new double[countWeights];
        for (int i = 0; i < countWeights; i++) {
            weights[i] = this.weights.get(i);
        }
        return weights;
    }

    public int getCountWeights() {
        return countWeights;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }

    public void updateWeight(int index, double value) {
        weights.set(index, value);
    }

    public void calculateNewValue(ArrayList<Node> prevNodes) {
        double newValue = IntStream.range(0, prevNodes.size()).parallel().mapToDouble(
                j -> weights.get(j) * prevNodes.get(j).getActivationValue()).sum();
        newValue += bias;
        this.zValue = newValue;
        this.activationValue = activationFunction.function(newValue);
    }

    public double getzValue() {
        return zValue;
    }

    public double getActivationValue() {
        return activationValue;
    }

    public void setActivationValue(double activationValue) {
        this.activationValue = activationValue;
    }
}
