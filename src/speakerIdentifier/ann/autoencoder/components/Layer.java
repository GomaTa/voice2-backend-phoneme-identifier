package speakerIdentifier.ann.autoencoder.components;

import speakerIdentifier.ann.activationFunctions.IActivationFunction;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 08.02.2016.
 */
public class Layer {
    private final int id;
    private final ArrayList<Node> nodes = new ArrayList<>();
    private final int countNodes;
    private Layer prevLayer;
    private int countNodesPrevLayer;
    private final IActivationFunction activationFunction;

    public Layer(int id, int countNodes, IActivationFunction activationFunction) {
        this.id = id;
        this.countNodes = countNodes;
        this.activationFunction = activationFunction;
    }

    public void build(Layer prevLayer, int countNodesPrevLayer) {
        // reference to previous layer
        this.prevLayer = prevLayer;
        this.countNodesPrevLayer = countNodesPrevLayer;
        // init nodes
        IntStream.range(0, countNodes).forEach(i -> this.nodes.add(new Node(i, countNodesPrevLayer, activationFunction)));
    }

    public double[] calculateActivations() {
        if (prevLayer != null) {
            ArrayList<Node> prevNodes = prevLayer.getNodes();
            IntStream.range(0, countNodes).parallel().forEach(j -> nodes.get(j).calculateNewValue(prevNodes));
        }
        return getActivations();
    }

    public double[] getActivations() {
        double[] values = new double[countNodes];
        for (int i = 0; i < countNodes; i++) {
            values[i] = nodes.get(i).getActivationValue();
        }
        return values;
    }

    public double[] getZs() {
        return IntStream.range(0, countNodes).parallel().mapToDouble(j -> nodes.get(j).getzValue()).toArray();
    }

    public void setValues(final double[] inputData) {
        IntStream.range(0, countNodes).parallel().forEach(j -> nodes.get(j).setActivationValue(inputData[j]));
    }

    public double[][] getWeights() {
        double[][] weights = new double[countNodes][countNodesPrevLayer];
        for (int i = 0; i < countNodes; i++) {
            weights[i] = nodes.get(i).getWeights();
        }
        return weights;
    }

    public int getCountNodesPrevLayer() {
        return countNodesPrevLayer;
    }

    public int getId() {
        return id;
    }

    public Layer getPrevLayer() {
        return prevLayer;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public int getCountNodes() {
        return countNodes;
    }

    public IActivationFunction getActivationFunction() {
        return activationFunction;
    }

    public double[] getBiases() {
        return IntStream.range(0, countNodes).parallel().mapToDouble(j -> nodes.get(j).getBias()).toArray();
    }
}
