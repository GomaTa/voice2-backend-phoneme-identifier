package speakerIdentifier.ann.autoencoder;

import speakerIdentifier.ann.activationFunctions.IActivationFunction;
import speakerIdentifier.ann.autoencoder.components.Layer;

/**
 * Created by GomaTa on 15.06.2016.
 */
public interface IAutoencoder {

    public void build();

    public double[] feedForward(double[] inputValues);

    public double[] encode(double[] input);

    public void addLayer(int id, int countNodes, IActivationFunction activationFunction);

    public Layer getLayer(int index);

    public Layer getOutputLayer();

    public Layer getInputLayer();

    public Layer getEncodeLayer();

    public boolean isBuild();
}
