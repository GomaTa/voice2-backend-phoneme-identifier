package speakerIdentifier.ann.autoencoder;


import speakerIdentifier.ann.activationFunctions.Logistic;
import speakerIdentifier.ann.activationFunctions.TanH;
import speakerIdentifier.audio.AudioStreamReader;
import speakerIdentifier.audio.framing.Framer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

/**
 * Created by GomaTa on 21.04.2016.
 */
public class AudioAutoencoder extends Autoencoder{
    private final int numInOutLayer;
    private final int numMidLayer;

    public AudioAutoencoder() {
        // Layer Info
        numInOutLayer = 2400;
        numMidLayer = 500;

        // Init Autoencoder
        addLayer(1, numInOutLayer, new Logistic());
        addLayer(2, numMidLayer, new Logistic());
        addLayer(3, numInOutLayer, new TanH());
        build();
    }

    public int getNumInOutLayer() {
        return numInOutLayer;
    }

    public double[] encode(File audiofile) {
        double[] coefficients = new double[numMidLayer];
        double[][] framedData = framing(audiofile, numInOutLayer, numInOutLayer / 2);
        final int n = framedData.length;
        double[][] outputData = new double[n][];
        // calculate autoencoder values
        for (int i = 0; i < framedData.length; i++) {
            outputData[i] = encode(framedData[i]);
        }
        //calculate average
        for (int i = 0; i < coefficients.length; i++) {
            double coefficient = 0.0;
            for (int j = 0; j < n; j++) {
                coefficient += outputData[j][i];
            }
            coefficients[i] = coefficient / Double.valueOf(n);
        }
        return coefficients;
    }

    private double[][] framing(File audioFile, final int windowSampleLength, final int windowSampleStep) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile);
            AudioStreamReader reader = new AudioStreamReader(audioInputStream);
            //return automatically normalised double values
            double[] samples = reader.readSamples();
            final int sampleRate = (int) reader.getSampleRate();
            Framer framer = new Framer();
            return framer.framing(samples, sampleRate, windowSampleLength, windowSampleStep);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
