package speakerIdentifier.ann.autoencoder;

import speakerIdentifier.ann.activationFunctions.IActivationFunction;
import speakerIdentifier.ann.autoencoder.components.Layer;

import java.util.ArrayList;

public class Autoencoder implements IAutoencoder {
    private boolean isBuild = false;
    private ArrayList<Layer> layers = new ArrayList<>();
    private Layer inputLayer;
    private Layer encodeLayer;
    private Layer outputLayer;
    private int countLayers = 0;
    private final boolean isDebug = true;

    public void build() {
        for (int i = 0; i < countLayers; i++) {
            Layer layer = layers.get(i);
            if (i > 0) {
                Layer prevLayer = layers.get(i - 1);
                layer.build(prevLayer, prevLayer.getCountNodes());
            } else {
                layer.build(null, 0);
            }
        }
        // set shortcuts for layers
        inputLayer = layers.get(0);
        encodeLayer = layers.get((countLayers - 1) / 2);
        outputLayer = layers.get(countLayers - 1);
        // build finished
        isBuild = true;
        if (isDebug) {
            System.out.println("[Debug] Built successfull.");
        }
    }


    public double[] feedForward(double[] inputValues) {
        if (isBuild) {
            inputLayer.setValues(inputValues);
            for (int i = 1; i < countLayers; i++) {
                layers.get(i).calculateActivations();
            }
            return outputLayer.getActivations();
        } else {
            //log("Autoencoder not built.");
            return null;
        }
    }

    public double[] encode(double[] input) {
        if (isBuild) {
            inputLayer.setValues(input);
            return encodeLayer.calculateActivations();

        } else {
            //log("Autoencoder not built.");
            return null;
        }
    }

    public void addLayer(int id, int countNodes, IActivationFunction activationFunction) {
        layers.add(new Layer(id, countNodes, activationFunction));
        countLayers++;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbLayer = new StringBuilder();
        StringBuilder sbNodes = new StringBuilder();
        for (int i = 0; i < countLayers; i++) {
            sbNodes.append(layers.get(i));
        }
        sb.append(sbLayer.toString() + "\n");
        sb.append(sbNodes.toString());
        return sb.toString();
    }

    public Layer getLayer(int index) {
        return layers.get(index);
    }

    public Layer getOutputLayer() {
        return outputLayer;
    }

    public Layer getInputLayer() {
        return inputLayer;
    }

    public Layer getEncodeLayer() {
        return encodeLayer;
    }

    public int getCountLayers() {
        return countLayers;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public boolean isBuild() {
        return isBuild;
    }


}
